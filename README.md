# gas-power-benchmark

This is a collection of parameters of a combined gas and power network simulation problem.

benchmarkdata.mat is a file loadable by Octave or Matlab.
which contains a struct (benchmarkdata) that is organized as follows:

All components mentioned in the paper are found together with parameters.
All physical parameters have a value and a corresponding unit.
Pipes and power nodes contain a struct named "initial" which contains initial values.
All sinks and sources of the gas network as well as power nodes contain a struct named "boundary" which sets boundary conditions.
Note that the boundary structs of all power nodes contain entries for all variables,
namely P,Q,V and phi, although only two are actually set for each node as is needed for the powerflow equations.
This is due to restrictions of Octave/Matlab itself.
The other variables are "empty" in the sense of the isempty() function of Octave.

For results see our paper https://arxiv.org/abs/2002.02416 or write an email to fokken@uni-mannheim.de for the result data.
